console.log("Happy New Year!");

//NEW SECTION - WHAT ARE OBJECTS?

// An object is a collection of related data and/or functionality.

// ANALOGY:
 // if CSS ==> Everything is a box.

 // JS ==> MOST things are objects.

 // Example:

//[HOW TO CREATE/INSTANTIATE AN OBJECT IN JS]
	 // 1. Curly Braces
 		// let cellphone = {}

 	// 2. Object Constructor / Object Initializer
 		// let cellphone = object()


// 1st approach CURLY BRACES
 // let cellphone = {
 // 	color: "White",
 // 	weight: "115 grams",
 // 	// an object can also contain functions
 // 	alarm: function() {
 // 		console.log('Alarm is buzzing');
 // 	},
 // 	ring: function() {
 // 		console.log('Cellphone is Ringing');
 // 	}
 // }

 // 2nd approach

 // 	let cellphone = object({
 // 		color: "White",
 // 		weight: "115 grams",
 // 		// an object can also contain functions
 // 		alarm: function() {
 // 		     console.log('Alarm is buzzing');
 // 		},

 // 		ring: function() {
 // 			console.log('Cellphone is Ringing');
 // 		}
 // 	})

 // console.log(cellphone);


// SYNTAX: function DesiredBlueprintName(argN) {
    // this.argN = valueNiArgN
// }

function Laptop(name, manufactureDate, color) {
    this.pangalan = name;
    this.createdOn = manufactureDate;
    this.kulay = color;
}

// This ==> keyword will allow us to assign new object properties by associating the received values.

// new ==> this keyword is use to create an instance of a new object.

let item1 = new Laptop("Lenovo", "2008", "black");
let item2 = new Laptop("Asus", "2015", "Pink");

console.log(item1);
console.log(item2);

// Create a function that will create multiple instances of a pokemon

function PokemonAnatomy(name, type, level, health) {
    this.pokemonName = name;
    this.pokemonLevel = level;
    this.pokemonType = type;
    this.pokeHealth = 80 * level;
    this.attack = function() {
        console.log("Pokemon Tackle");
    }
}


let pikachu = new PokemonAnatomy('Pikachu', 'Electric', 3);
let ratata = new PokemonAnatomy('Ratata', 'Ground', 2);
let onyx = new PokemonAnatomy('Onyx', 'Rock', 8);
let meowth = new PokemonAnatomy('Meowth', 'Normal', 9);
let snorlax = new PokemonAnatomy('Snorlax', 'Normal', 9);


console.log(pikachu);

// [ACCESS ELEMENTS/PROPERTIES INSIDE AN OBJECT]

// 1. Dot Notation (.)
console.log(pikachu.pokeHealth);

// 2. Square Brackets ([])
console.log(snorlax['pokemonType']);